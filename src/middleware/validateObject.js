const populateInvalidProperties = (obj, requiredProperties) => {
  let invalidProperties = requiredProperties.filter(property => !obj.hasOwnProperty(property));
  return invalidProperties;
}

const isValidObject = (obj, requiredProperties) => {
  let isValid = requiredProperties.every(property => obj.hasOwnProperty(property));
  return isValid;
}

const validateObject = (req, res, next) => {
  const requiredProperties = ["name", "email", "content"];

  if (isValidObject(req.body, requiredProperties)){
    next();
  } else {
      let invalidProperties = populateInvalidProperties(req.body, requiredProperties);
      return res.status(400).json({message: "validation error", invalid: invalidProperties});
  }
}

export { validateObject, isValidObject, populateInvalidProperties }