import express from "express"
import db from "../../database/connection.js"

const router = express.Router()

/**Patient Billing Profile - Single Patient Billing Profile**/
router.get("/:id", (req, res) => {
  db.query(
    `SELECT * FROM billing WHERE patientID=${req.params.id}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});

/**Add Patient Billing**/
router.post("/:id", (req, res) => {
  db.query(
    "INSERT INTO billing (patientID, information, amount, paymentMethod, status) VALUES ( ?, ?, ?, ?, ?)",
    [req.body.patientID, req.body.information ,req.body.amount ,req.body.paymentMethod, req.body.status],
    function (error, results, fields) {
      if (error) throw error;
      return res.status(201).send(results);
    }
  );
});

/**To Update/Edit Patient Billing Profile**/
router.put("/:id", (req, res) => {
  const { information, amount, paymentMethod, status } = req.body;
  db.query(
    `UPDATE billing SET information="${information}",amount="${amount}", paymentMethod="${paymentMethod}",status ="${status}" WHERE invoice=${req.body.invoice}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});

/**To Delete Patient Billing Profile**/
router.delete("/:id", (req, res) => {
  db.query(
    `DELETE FROM billing WHERE invoice=${req.body.invoice}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});

export default router