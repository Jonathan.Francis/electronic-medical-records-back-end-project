import express from "express"
import jwt from "jsonwebtoken"

import validateLogin from "../middleware/validateLogin.js"
import getUser from "../dbHandlers/getUser.js"

const router = express.Router()

router.post('/', validateLogin, async (req, res) => {

  try {
    const email = req.body.email;
    const token = jwt.sign({email}, process.env.JWT_SECRET, {expiresIn: '60m'})
    const user = await getUser("email", email)
    return res.status(200).json({token, userID:user[0].userID, title:user[0].title, firstName:user[0].firstName, lastName:user[0].lastName, email:user[0].email, isAdmin:user[0].isAdmin })
  } catch (err) {
      console.log(err)
      return next(err)
  }
})

export default router