import express from "express"

import updateUser from "../dbHandlers/updateUser.js"

const router = express.Router()

router.patch('/:id', async (req, res) => {
  try {
    let result = await updateUser(req.body, req.params.id)
    return res.status(200).json(result.message)
  } catch (err) {
    return res.status(404).json(err)
  }
  
}) 

export default router