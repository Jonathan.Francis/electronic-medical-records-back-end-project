import express from "express"
import db from "../../database/connection.js"


const router = express.Router()

/**Find Entire List of Patients - Master List**/
router.get("/", function (req, res) {
    db.query(
      "SELECT * FROM patient",
      function (error, results, fields) {
        if (error) throw error;
        return res.status(200).send(results);
      }
    );
  });
  
  /**Find Patient - Search for a Single Patient**/
 router.get("/search/:healthnum", function (req, res) {
    let healthNum = req.params.healthnum
    db.query(
      `SELECT patientID FROM patient WHERE healthCardNumber="${healthNum}"`,
      function (error, results, fields) {
        if (error) throw error;
        return res.status(200).send(results[0]);
      }
    );
  });
  
/**Patient Profile - Single Patient Profile**/
router.get("/:id", (req, res) => {
    db.query(
      `SELECT * FROM patient WHERE patientID=${req.params.id}`,
      function (error, results, fields) {
        if (error) throw error;
        return res.status(200).send(results);
      }
    );
  });
  

/**To Update/Edit Patient Profile**/
  router.put("/:id", (req, res) => {
    const { healthCardNumber, firstName, lastName, gender, email, phoneNumber } = req.body;
    db.query(
      `UPDATE patient SET healthCardNumber="${healthCardNumber}",firstName="${firstName}",lastName="${lastName}", gender ="${gender}",email ="${email}", phoneNumber="${phoneNumber}" WHERE patientID=${req.params.id}`,
      function (error, results, fields) {
        if (error) throw error;
        return res.status(200).send(results);
      }
    );
  });
  
/**To Delete Patient Profile**/
  router.delete("/:id", (req, res) => {
    db.query(
      `DELETE FROM patient WHERE patientID=${req.params.id}`,
      function (error, results, fields) {
        if (error) throw error;
        return res.status(200).send(results);
      }
    );
  });

  export default router