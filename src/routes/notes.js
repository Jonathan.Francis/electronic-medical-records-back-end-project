import express from "express"
import db from "../../database/connection.js"

const router = express.Router()

/**Patient Notes Profile - Single Patient Notes Profile**/
router.get("/:id", (req, res) => {
  db.query(
    `SELECT * FROM notes WHERE patientID=${req.params.id} ORDER BY date DESC`,
    function (error, results, date) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});

/**Add Patient Notes Record**/
router.post("/:id", (req, res) => {
  db.query(
    "INSERT INTO notes (patientID, date, information) VALUES (?, CURRENT_TIMESTAMP, ?)",
    [req.body.patientID , req.body.information],
    function (error, results, fields) {
      if (error) throw error;
      return res.status(201).send(results);
    }
  );
});

/**To Update/Edit Patient Notes Record**/
/*router.put("/:id", (req, res) => {
  const { date, information } = req.body;
  db.query(
    `UPDATE notes SET date="${date}",information ="${information}" WHERE notesID=${req.body.notesID}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});*/

/**To Delete Patient Notes Record**/
router.delete("/:id", (req, res) => {
  db.query(
    `DELETE FROM notes WHERE notesID=${req.body.notesID}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});
export default router