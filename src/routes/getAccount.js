import express from "express"
import searchUsers from "../dbHandlers/searchUsers.js";
import getUser from "../dbHandlers/getUser.js";
import getAll from "../dbHandlers/getAll.js";

const router = express.Router()

router.get('/all', async (req, res) => {
  try {
    let users = await getAll("user")
    return res.status(200).json(users)
  } catch (err) {
    return res.status(404).send(err)
  }

})

router.post('/search', async (req, res) => {
  try {
    let users = await searchUsers(req.body)
    return res.status(200).send(users)
  } catch (err) {
    return res.status(404).send(err)
  }
})

router.get('/:id', async (req, res) => {
  try {
    const id = req.params.id;
    const user = await getUser("userID", id)
    return res.status(200).json(user[0])
  } catch (err) {
    return res.status(404).send(err)
  }

})

export default router