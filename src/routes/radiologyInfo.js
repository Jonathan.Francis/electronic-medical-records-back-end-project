import express from "express"
import db from "../../database/connection.js"

const router = express.Router()

/**Patient radiology Profile - Single Patient radiology Profile**/
router.get("/:id", (req, res) => {
  db.query(
    `SELECT * FROM radiology WHERE patientID=${req.params.id}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});


export default router
