import express from "express"
import db from "../../database/connection.js"


const router = express.Router()


/**Add Patient**/
router.post("/", (req, res) => {
    db.query(
      "INSERT INTO patient (healthCardNumber,firstName,lastName,gender,email,birthDate,phoneNumber) VALUES (?, ?, ?, ?, ?, ?, ?)",
      [req.body.healthCardNumber, req.body.firstName,req.body.lastName,req.body.gender,req.body.email,req.body.birthDate,req.body.phoneNumber],
      function (error, results, fields) {
        if (error) throw error;
        return res.status(201).send(results);
      }
    );
  });
  

  export default router;