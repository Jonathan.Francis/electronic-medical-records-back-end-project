import express from "express"
import bcrypt from "bcrypt"
import validateUser  from "../middleware/validateUser.js"
import addUser from "../dbHandlers/addUser.js"

const router = express.Router()

  let createHash = async (password) => {
    try {
      const hash = await bcrypt.hash(password, 10)
      return hash
    } catch (err) {
      return res.status(400).send(err)
    }
  }

router.post('/', validateUser, async (req, res) => {

  try {
    let newUser = await createHash(req.body.password).then(hash => {
      return {...req.body, password: hash}
    })
    await addUser(newUser)
    return res.status(201).json(newUser)
  } catch (err) {
    return res.status(400).send(err)
  }
  
}) 

export default router