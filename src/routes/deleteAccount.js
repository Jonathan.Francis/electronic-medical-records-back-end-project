import express from "express"
import deleteUser from "../dbHandlers/deleteUser.js";


const router = express.Router()

router.delete('/:id', async (req, res) => {
  try {
    const id = req.params.id;
    const response = await deleteUser("userID", id)
    return res.status(200).json(response)
  } catch (err) {
    console.log(err)
    return res.status(404).send(err)
  }

})

export default router