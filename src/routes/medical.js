import express from "express"
import db from "../../database/connection.js"

const router = express.Router()

/**Patient Medical Profile - Single Patient Medical Profile**/
router.get("/:id", (req, res) => {
  db.query(
    `SELECT * FROM medical WHERE patientID=${req.params.id}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});

/**Add Patient Medical Record**/
router.post("/:id", (req, res) => {
  db.query(
    "INSERT INTO medical (patientID, allergies, prescriptions, immunizationStatus, diagnosis) VALUES (?, ?, ?, ?, ?)",
    [req.body.patientID, req.body.allergies ,req.body.prescriptions ,req.body.immunizationStatus, req.body.diagnosis],
    function (error, results, fields) {
      if (error) throw error;
      return res.status(201).send(results);
    }
  );
});

/**To Update/Edit Patient Medical Record**/
router.put("/:id", (req, res) => {
  const { allergies, prescriptions, immunizationStatus, diagnosis } = req.body;
  db.query(
    `UPDATE medical SET allergies="${allergies}",prescriptions="${prescriptions}", immunizationStatus="${immunizationStatus}",diagnosis ="${diagnosis}" WHERE medicalRecord=${req.body.medicalRecord}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});

/**To Delete Patient Medical Record**/
router.delete("/:id", (req, res) => {
  db.query(
    `DELETE FROM medical WHERE medicalRecord=${req.body.medicalRecord}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});

export default router