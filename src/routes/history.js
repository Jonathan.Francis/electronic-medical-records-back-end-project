import express from "express"
import db from "../../database/connection.js"

const router = express.Router()

/**Patient Notes Profile - Single Patient Notes Profile**/
router.get("/:id", (req, res) => {
  db.query(
    `SELECT * FROM history WHERE patientID=${req.params.id} ORDER BY date DESC`,
    function (error, results, date) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});

/**Add Patient Notes Record**/
router.post("/:id", (req, res) => {
  db.query(
    "INSERT INTO history (patientID, date, information) VALUES (?, CURRENT_TIMESTAMP, ?)",
    [req.body.patientID , req.body.information],
    function (error, results, fields) {
      if (error) throw error;
      return res.status(201).send(results);
    }
  );
});

export default router