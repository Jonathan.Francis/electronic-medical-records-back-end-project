import db from "../../database/connection.js"

const searchUsers = (data) => {

  //turn given fields and values into separate arrays
  let keys = Object.keys(data)
  let values = Object.values(data)

  const setString = () => {
      //create an array for the concatenated MySQL query, add AND between requests if more than one field to check
      let setArray = keys.map((key, i) => `${key} LIKE "%${values[i]}%" ${(i !== (values.length -1)) ? "AND" : ""}`)
      //turn array into string
      let setString = setArray.join(' ')
      return setString

  }

  return new Promise((resolve, reject) => {
      db.query(`SELECT * FROM user WHERE ${setString()}`, 
        (error, results) => {
          if (error){
            console.log(error)
            return reject(error)
          } else {
            return resolve(results)
          }
        }
      )
    }
  )
}

export default searchUsers