import db from "../../database/connection.js"

const getAll = (database) => {
  return new Promise((resolve, reject) => {
      db.query(`SELECT * FROM ${database}`, 
        (error, results) => {
          if (error){
            return reject(error)
          } else {
            return resolve(results)
          }
        }
      )
    }
  )
}

export default getAll