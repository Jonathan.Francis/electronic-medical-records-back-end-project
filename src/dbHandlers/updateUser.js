import db from "../../database/connection.js"

const updateUser = async (data, id) => {

  let keys = Object.keys(data)
  let values = Object.values(data)

  let setString = keys.map((key, i) => `${key} = "${values[i]}"`)

  return new Promise ((resolve, reject) => {
    db.query(
      `UPDATE user 
      SET ${setString} 
      WHERE userID = ${id};`,

      (error, results) => {
        if (error){
          return reject(error)
        } else {
          return resolve(results)
        } 
      }
    )
  })
}

export default updateUser

      
      