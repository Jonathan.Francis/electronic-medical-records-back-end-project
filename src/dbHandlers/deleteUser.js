import db from "../../database/connection.js"

const deleteUser = (field, value) => {
  return new Promise((resolve, reject) => {
      db.query(`DELETE FROM user WHERE ${field} = ${value}`, 
        (error, results) => {
          if (error){
            return reject(error)
          } else {
            return resolve(results)
          }
        }
      )
    }
  )
}

export default deleteUser