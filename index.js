import express from "express"
import loginRoutes from "./src/routes/login.js"
import createAccountRoutes from "./src/routes/createAccount.js"
import billingRoutes from "./src/routes/billing.js"
import medicalRoutes from "./src/routes/medical.js"
import notesRoutes from "./src/routes/notes.js"
import addPatientRoutes from "./src/routes/addPatient.js"
import findPatientRoutes from "./src/routes/findPatient.js"
import cors from "cors"
import updateAccount from './src/routes/updateAccount.js'
import getAccount from './src/routes/getAccount.js'
import deleteAccout from './src/routes/deleteAccount.js'
import radiologyRoutes from "./src/routes/radiologyInfo.js"
import testRoutes from "./src/routes/testInfo.js"
import historyRoutes from "./src/routes/history.js"

const app = express();
const port = process.env.PORT || 3001

// parse application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: false }))
 
// parse application/json
app.use(express.json())
app.use(cors())

app.use('/login', loginRoutes)
app.use('/create-account', createAccountRoutes)
app.use('/update-account', updateAccount)
app.use('/find-account', getAccount)
app.use('/delete-account', deleteAccout)
app.use('/billing', billingRoutes)
app.use('/medical', medicalRoutes)
app.use('/notes', notesRoutes)
app.use('/radiologyInfo', radiologyRoutes)
app.use('/testInfo', testRoutes)
app.use('/addPatient', addPatientRoutes)
app.use('/findPatient', findPatientRoutes)
app.use('/history', historyRoutes)

app.get("/", function (req, res) {
  res.send("Hello World");
});

const PORT = 3001;
app.listen(PORT, () => console.log(`Server started on http://localhost:${port}`));