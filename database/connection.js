import mysql from "mysql"
const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "password",
  database: "emr",
});

connection.connect(function (err){
 if (err){
     console.log(err);
 } else{
     console.log("MySQL database is connected");
 }
});

export default connection;
