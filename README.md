# Electronic Medical Records Back End Web Application

1. Link to Front End Web Application - https://gitlab.com/Jonathan.Francis/electronic-medical-records-project-frontend
2. Link to Back End Web Application - https://gitlab.com/Jonathan.Francis/electronic-medical-records-back-end-project
3. Link to Database - https://gitlab.com/Jonathan.Francis/electronic-medical-records-database-project
 

# Run this project using npm & MySQL
1. To run this project you will need to clone this repo using SSH or HTTP
2. Open the folder in VS Code or applicable source-code editor with access to terminal
3. Create a `.env` file with a `JWT_SECRET`
4. Also in your `.env` file, choose your port to be 3001 for this project specifically, that you will run your backend with `PORT`
5. Have a look at .envExample file to see how to create your `.env` file 
6. Do NOT run port on 3000, as the Front End React project will be running on port 3000. For this specific Back End Project we will be running the port on 3001.
7. When in the terminal, use npm install to install node dependencies - `npm install`
8. Then run in the terminal - `npm start` to start the server using nodemon (Note your Back End will crash until MySQL is connected see step below & Database Link to set up the Database)
9. Now set up your Database - follow instructions to create your DATABASE & add the required Tables/Values - see README file in the link here - https://gitlab.com/Jonathan.Francis/electronic-medical-records-database-project to get started.
10. Once your Database has been set up , you can now try in your terminal - `npm start` to start the server using nodemon
11. Once Database/Back End are connected - you can login with the credentials provided on the README file - from the Front End link above or see below - step 12.
12. To login email is "johndoe@gmail.com", password is "password".
Reminder For the login credientials to work , please follow the instructions from the Back End & Database Link above, will need the Back End set up, along with the Database details imported into MySQL Workbench.


# POSTMAN Details :
- to create a user, make a `POST` request to /create-account. Request must contain firstName, lastName, email (must be valid email), phone, password (longer than 8 characters), and isAdmin (true or false).
- to validate a user login, make a `POST` request to /login. Request must contain email and password.

# SQL Queries are located in the following locations:
File: src\dbHandlers\updateUser.js
12:       `UPDATE user 
13:       SET ${setString} 
14:       WHERE userID = ${id};`

File: src\routes\testInfo.js
9:     `SELECT * FROM testResults WHERE patientID=${req.params.id}`,

File: src\routes\radiologyInfo.js
9:     `SELECT * FROM radiology WHERE patientID=${req.params.id}`,

File: src\routes\notes.js
9:     `SELECT * FROM notes WHERE patientID=${req.params.id} ORDER BY date DESC`,

File: src\routes\notes.js
20:     "INSERT INTO notes (patientID, date, information) VALUES (?, CURRENT_TIMESTAMP, ?)",
21:     [req.body.patientID , req.body.information],

File: src\routes\notes.js
44:     `DELETE FROM notes WHERE notesID=${req.body.notesID}`,

File: src\routes\medical.js
9:     `SELECT * FROM medical WHERE patientID=${req.params.id}`,

File: src\routes\medical.js
20:     "INSERT INTO medical (patientID, allergies, prescriptions, immunizationStatus, diagnosis) VALUES (?, ?, ?, ?, ?)",
21:     [req.body.patientID, req.body.allergies ,req.body.prescriptions ,req.body.immunizationStatus, req.body.diagnosis],

File: src\routes\medical.js
33:     `UPDATE medical SET allergies="${allergies}",prescriptions="${prescriptions}", immunizationStatus="${immunizationStatus}",diagnosis ="${diagnosis}" WHERE medicalRecord=${req.body.medicalRecord}`,

File: src\routes\medical.js
44:     `DELETE FROM medical WHERE medicalRecord=${req.body.medicalRecord}`,

File: src\dbHandlers\getUser.js
5:       `SELECT * FROM user where ${field} IN("${value}")` 

File: src\routes\history.js
9:     `SELECT * FROM history WHERE patientID=${req.params.id} ORDER BY date DESC`,

File: src\routes\history.js
20:     "INSERT INTO history (patientID, date, information) VALUES (?, CURRENT_TIMESTAMP, ?)",
21:     [req.body.patientID , req.body.information],

File: src\dbHandlers\getAll.js
5:       `SELECT * FROM ${database}`

File: src\dbHandlers\searchUsers.js
19:       `SELECT * FROM user WHERE ${setString()}`

File: src\dbHandlers\getUser.js
5:       `SELECT * FROM user where ${field} IN("${value}")`

File: src\routes\findPatient.js
10:       "SELECT * FROM patient"

File: src\routes\findPatient.js
22:       `SELECT patientID FROM patient WHERE healthCardNumber="${healthNum}"`

File: src\routes\findPatient.js
33:       `SELECT * FROM patient WHERE patientID=${req.params.id}`

File: src\routes\findPatient.js
46:       `UPDATE patient SET healthCardNumber="${healthCardNumber}",firstName="${firstName}",lastName="${lastName}", gender ="${gender}",email ="${email}", phoneNumber="${phoneNumber}" WHERE patientID=${req.params.id}`

File: src\routes\findPatient.js
57:       `DELETE FROM patient WHERE patientID=${req.params.id}`

File: src\dbHandlers\deleteUser.js
5:       `DELETE FROM user WHERE ${field} = ${value}`

File: src\dbHandlers\addUser.js
7:       `INSERT INTO user (title, firstName, lastName, email, password, phoneNumber, isAdmin) VALUE ("${title}""${firstName}", "${lastName}", "${email}", "${password}", "${phone}", "${Number(isAdmin)}")`

File: src\routes\billing.js
9:     `SELECT * FROM billing WHERE patientID=${req.params.id}`

File: src\routes\billing.js
20:     "INSERT INTO billing (patientID, information, amount, paymentMethod, status) VALUES ( ?, ?, ?, ?, ?)",
21:     [req.body.patientID, req.body.information ,req.body.amount ,req.body.paymentMethod, req.body.status],

File: src\routes\billing.js
33:     `UPDATE billing SET information="${information}",amount="${amount}", paymentMethod="${paymentMethod}",status ="${status}" WHERE invoice=${req.body.invoice}`

File: src\routes\billing.js
44:     `DELETE FROM billing WHERE invoice=${req.body.invoice}`

File: src\routes\addPatient.js
11:       "INSERT INTO patient (healthCardNumber,firstName,lastName,gender,email,birthDate,phoneNumber) VALUES (?, ?, ?, ?, ?, ?, ?)",
12:       [req.body.healthCardNumber, req.body.firstName,req.body.lastName,req.body.gender,req.body.email,req.body.birthDate,req.body.phoneNumber],






















